package GridGame_NonNetwork;

/**
 * Created by spaism on 2/19/15.
 */
public class GameCaller {
    public static void main(String[] args){

        Player player1 = new Player(1);
        Player player2 = new Player(2);

        Game game = new Game(player1, player2);

        game.startGame();
    }
}
